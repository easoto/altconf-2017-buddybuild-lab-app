//
//  ViewController.swift
//  RomanNumeralCalculator
//
//  Created by Chris on 2016-09-07.
//  Copyright © 2016 buddybuild. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var crashItOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Add a 5-tap gesture so we can simulate a crash
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 5
        tap.addTarget(self, action: #selector(ViewController.CrashIt))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
    }
    
    /**
     This is just a function that will crash for demo purposes.
     */
    func CrashIt(){
        crashItOn = !crashItOn
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var total: Int = 0
    var signOfNextOperation: Int = 1;

    func update() {
        //resultLabel.text = String(total)
        resultLabel.text = toRoman(total)
    }


    func handleButton(_ value: Int) {
        total += value * signOfNextOperation
        update()
    }

    @IBOutlet weak var buttonI: UIButton!
    @IBOutlet weak var buttonV: UIButton!
    @IBOutlet weak var buttonX: UIButton!
    @IBOutlet weak var buttonL: UIButton!
    @IBOutlet weak var buttonC: UIButton!
    @IBOutlet weak var buttonD: UIButton!
    @IBOutlet weak var buttonM: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var buttonClear: UIButton!

    @IBAction func actionI(_ sender: AnyObject) {
        handleButton(1)
    }

    @IBAction func actionV(_ sender: AnyObject) {
        handleButton(5)
    }

    @IBAction func actionX(_ sender: AnyObject) {
        handleButton(10)
    }

    @IBAction func actionL(_ sender: AnyObject) {
        handleButton(50)
    }


    @IBAction func actionC(_ sender: AnyObject) {
        handleButton(100)
    }

    @IBAction func actionD(_ sender: AnyObject) {
        if crashItOn {
            [0][1]
        } else {
            handleButton(500)
        }
    }

    @IBAction func actionM(_ sender: AnyObject) {
        handleButton(1000)
    }

    @IBAction func actionPlus(_ sender: AnyObject) {
        signOfNextOperation = 1
    }

    @IBAction func actionMinus(_ sender: AnyObject) {
        signOfNextOperation = -1
    }

    @IBAction func actionClear(_ sender: AnyObject) {
        total = 0
        update()
    }

}

