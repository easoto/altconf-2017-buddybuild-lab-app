# AltConf 2017: buddybuild lab app
**by Eric Soto, Lead Engineer, MJD Interactive**

## Summary

The purpose of the repo is to provide a user a way to experiment with buddybuild with a "ready-made" app. buddybuild allows developers to easily implement an automated build environment for their mobile apps.

[Learn more about buddybuild](https://www.buddybuild.com/) by visiting [buddybuild.com](https://www.buddybuild.com/).

This repo is a demo cloned from [christopherstott/RomanNumeralCalculator](https://github.com/christopherstott/RomanNumeralCalculator). I did not originally write this code, nor did I vet its origin (other than the above repo as the source) and I make no claims to rights related to this code. Use of this code is at your own risk and I make no warranties, expressed or implied about the proper operation or correctness of this code.

Furthermore, I am not affiliated with buddybuild and this repo is not necessarily sanctioned by buddybuild. 

However, I am a huge fan and customer of buddybuild!

## Change History

- 5/27/2017: 
    - Updated the project to work with Xcode 8.3 and Swift 3 syntax. 
    - Removed the buddybuild SDK which the above repo did include. (Since we want to allow the testing user the ability to add that on their own.)

## About Eric

I'm Eric Soto and I'm a Software Engineer specializing in iOS Apps, Mobile Apps, APIs and AWS Infrastructure.

Presently, I work as a Lead Engineer for [MJD Interactive](https://www.mjdinteractive.com), a Digital Innovation Agency in San Diego, CA. MJD focuses on projects that impact tens of thousands of end-users.

I am also the founder of and lead instructor at [Learn-Swift-Programming](https://www.learn-swift-programming.com) where I help experienced developers wanting to transition to Swift.

If you have any comments or questions, please feel free to reach out to me.

Eric Soto

[ericsoto.net](https://www.ericsoto.net/)



